# https://docs.openstack.org/mitaka/user-guide/common/cli_install_openstack_command_line_clients.html
#
openstacksdk==0.99.0
python-openstackclient==5.8.0
python-keystoneclient==4.5.0 # identity service
python-novaclient==18.0.0 # compute service api
python-glanceclient==4.0.0 # image service
python-neutronclient==7.8.0 # networking service api
python-cinderclient==8.3.0 # block storage
python-magnumclient==3.6.0 # container service
python-barbicanclient==5.3.0 # key manager service
python-swiftclient==4.0.0 # object storage service 