# This module holds all relevant methods for migrating OpenStack Compute Services
__author__ = "Jan Frömberg"
__copyright__ = "Copyright 2022, Cloud and Heat Technologies GmbH"
__credits__ = ["Jan Frömberg"]
__license__ = "Apache License 2.0"
__version__ = "1.0.0"
__email__ = "jan.froemberg@cloudandheat.com"

import openstack
import OpenStackConnection as OSC
 
conn1 = OSC.Connection('gaiax-cloud-a')
conn2 = OSC.Connection('gaiax-cloud-b')

"""
Compute instances migration
"""
def migrateInstances():
    print("Kindly implement the instances migration for an OpenStack")

"""
Compute images migration
"""
def migrateImages():
    # conn1.listServers()
    print("Kindly implement the images migration for an OpenStack")

""" 
Compute keypais migration
"""
def migrateKeyPairs():
    print("Kindly implement the key pais migration for an OpenStack")

"""
Start all migration processes all together
"""
def startAll() :
    migrateInstances()
    migrateImages()
    migrateKeyPairs()