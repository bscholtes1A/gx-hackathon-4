##
# This is the openstack connection class
# https://docs.openstack.org/openstacksdk/latest/user/connection.html
#
__author__ = "Jan Frömberg"
__copyright__ = "Copyright 2022, Cloud and Heat Technologies GmbH"
__credits__ = ["Jan Frömberg"]
__license__ = "Apache License 2.0"
__version__ = "1.0.0"
__email__ = "jan.froemberg@cloudandheat.com"


from os import environ as env
import openstack
import keystoneclient.v2_0.client as ksclient
import glanceclient.v2.client as glclient
import novaclient.v2.client as nvclient
import neutronclient.v2_0.client as ntclient

"""
Initialize a os connection by a given name 'mycloud'
see clouds.yaml file for configuration
"""
class Connection:

    def __init__(self, mycloud) -> None:
        # Initialize and turn on debug logging
        openstack.enable_logging(debug=True)

        # Initialize connection
        self.conn = openstack.connect(cloud=mycloud)
    

    """
    return the connection
    """
    def getCon(self) -> openstack.connection.Connection :
        return self.conn


    """
    return an auth token of an openstack keystone instance
    """
    def getAuthToken() :
        keystone = ksclient.Client(auth_url=env['OS_AUTH_URL'],
                           username=env['OS_USERNAME'],
                           password=env['OS_PASSWORD'],
                           tenant_name=env['OS_TENANT_NAME'],
                           region_name=env['OS_REGION_NAME'])
        return keystone.auth_token


    """
    returns a glance image service of openstack
    """
    def getImageService() :
        keystone = ksclient.Client(auth_url=env['OS_AUTH_URL'],
                           username=env['OS_USERNAME'],
                           password=env['OS_PASSWORD'],
                           tenant_name=env['OS_TENANT_NAME'],
                           region_name=env['OS_REGION_NAME'])
        glance_endpoint = keystone.service_catalog.url_for(service_type='image')
        glance = glclient.Client(glance_endpoint, token=keystone.auth_token)
        return glance


    """
    returns a nova compute endpoint of openstack 
    """
    def getComputeEndpoint() :
        nova = nvclient.Client(auth_url=env['OS_AUTH_URL'],
                       username=env['OS_USERNAME'],
                       api_key=env['OS_PASSWORD'],
                       project_id=env['OS_TENANT_NAME'],
                       region_name=env['OS_REGION_NAME'])
        return nova

    
    """
    returns a neutron networking endpoint of openstack
    """
    def getNetworkingEndpoint() :
        neutron = ntclient.Client(auth_url=env['OS_AUTH_URL'],
                               username=env['OS_USERNAME'],
                               password=env['OS_PASSWORD'],
                               tenant_name=env['OS_TENANT_NAME'],
                               region_name=env['OS_REGION_NAME'])
        return neutron

    
    """
    close the connection
    """
    def close(self):
        self.conn.close()


    """
    list all servers of that given connection
    """
    def listServers(self):
        # List the servers
        for server in self.conn.compute.servers():
            print(server.to_dict())