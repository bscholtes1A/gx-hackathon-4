# This module holds all relevant methods for migrating OpenStack Compute Services
__author__ = "Jan Frömberg"
__copyright__ = "Copyright 2022, Cloud and Heat Technologies GmbH"
__credits__ = ["Jan Frömberg"]
__license__ = "Apache License 2.0"
__version__ = "1.0.0"
__email__ = "jan.froemberg@cloudandheat.com"

import openstack
import OpenStackConnection as OSC
 
conn1 = OSC.Connection('gaiax-cloud-a')
conn2 = OSC.Connection('gaiax-cloud-b')

"""
compute instances migration
"""
def migrateClusters():
    print("Kindly implement the cluster migration for an OpenStack")

""" 
compute images migration
"""
def migrateClusterTemplates():
    print("Kindly implement the cluster template migration for an OpenStack")

"""
Start all migration processes all together
"""
def startAll():
    migrateClusterTemplates()
    migrateClusters()